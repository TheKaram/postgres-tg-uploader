:rocket:import asyncio
import datetime
import os
import time
from pathlib import Path

import dotenv
from aiologger import Logger
from pyrogram import Client
from pyrogram.enums import ChatAction
from pyrogram.types import Message

dotenv.load_dotenv()


API_ID = os.environ.get("API_ID")
API_HASH = os.environ.get("API_HASH")
BOT_TOKEN = os.environ.get("BOT_TOKEN")
TELEGRAM_CHAT_ID = int(os.environ.get("CHAT_ID"))

DATABASE_NAME = "mangay"
CURRENT_DATE = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
DUMP_FILE_PATH = Path("/tmp") / f"{DATABASE_NAME}_{CURRENT_DATE}"

app = Client("db_uploader", api_id=API_ID, api_hash=API_HASH, bot_token=BOT_TOKEN)
logger = Logger.with_default_handlers(name="my-logger")


async def dump_database():
    # Dump the specified database to a file using custom PostgreSQL format and best compression
    await logger.debug(f"Dumping database to {DUMP_FILE_PATH}")
    process = await asyncio.create_subprocess_shell(
        f"pg_dump -Fc {DATABASE_NAME} > {DUMP_FILE_PATH}"
    )
    await process.wait()
    await logger.info("Database has been dumped successfully")


async def upload_to_telegram():
    # Upload backup file to Telegram via MTProto
    async def uploading_progress(current: int, total: int, msg: Message, start: float):
        diff = time.time() - start

        if round(diff % 10.00) == 0 or current == total:
            percent = f"{current * 100 / total:.1f}%"
            message = f"⬆️ Uploading Database Backup\nProgress: {percent}"
            # await msg.edit_text(message)
            await logger.info(message)

    await logger.debug("Sending chat action")
    await app.send_chat_action(TELEGRAM_CHAT_ID, ChatAction.UPLOAD_DOCUMENT)
    await logger.debug("Uploading")
    msg = await app.send_message(TELEGRAM_CHAT_ID, "Uploading Database Backup...")
    start = time.time()
    await app.send_document(
        TELEGRAM_CHAT_ID,
        str(DUMP_FILE_PATH),
        caption=f"📦 Backup file\n📅 Date: {CURRENT_DATE}",
        disable_notification=True,
        progress=uploading_progress,
        progress_args=(msg, start),
    )
    await msg.delete()
    await logger.info("File has been uploaded successfully")

    # Delete local backup file
    await logger.debug("Deleting temporary backup file")
    os.remove(DUMP_FILE_PATH)
    await logger.info("Temporary backup file has been deleted successfully")


async def main():
    await logger.debug("Running bot")
    async with app:
        await dump_database()
        await upload_to_telegram()


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
